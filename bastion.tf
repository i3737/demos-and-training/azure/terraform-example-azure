resource "azurerm_public_ip" "bastion" {
  name                = "bastionPublicIp"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  allocation_method   = "Static"

  tags = {
    environment = "demo"
  }
}
resource "azurerm_network_interface" "bastion" {
  name                = "bastion_if"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  ip_configuration {
    name                          = "pub_ip"
    subnet_id                     = azurerm_subnet.sub_a.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.bastion.id
  }
}

resource "azurerm_virtual_machine" "bastion" {
  name                             = "bastion"
  location                         = azurerm_resource_group.example.location
  resource_group_name              = azurerm_resource_group.example.name
  network_interface_ids            = [azurerm_network_interface.bastion.id]
  vm_size                          = "Standard_B2s"
  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }

  storage_os_disk {
    name              = "myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "bastion"
    admin_username = "adminuser"
    custom_data    = base64encode(file("custom_data_bastion.sh"))
  }
  os_profile_linux_config {
    disable_password_authentication = "true"
    ssh_keys {
      path     = "/home/adminuser/.ssh/authorized_keys"
      key_data = var.admin_ssh_key
    }
  }
  tags = {
    environment = "demo"
  }
}
