
resource "azurerm_linux_virtual_machine_scale_set" "example" {
  name                = "web-server-vmss"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  sku                 = "Standard_B2s"
  instances           = 2
  admin_username      = "adminuser"

  admin_ssh_key {
    username   = "adminuser"
    public_key = var.admin_ssh_key
  }

  custom_data = base64encode(file("custom_data_www.sh"))

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }

  network_interface {
    name                      = "example"
    primary                   = true
    network_security_group_id = azurerm_network_security_group.example.id

    ip_configuration {
      name                                         = "internal"
      primary                                      = true
      subnet_id                                    = azurerm_subnet.sub_a.id
      application_gateway_backend_address_pool_ids = [tolist(azurerm_application_gateway.web_ag.backend_address_pool).0.id]
    }
  }
}
