# The recommended to use the required_providers block.
# Specify the Azure Provider source and version
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}

# Configure the Microsoft Azure Resource Manager Provider
provider "azurerm" {
  features {}
}
