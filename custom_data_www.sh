#!/bin/bash

# update apt repo cache
apt update

# install ansible for future steps
apt install -y ansible

# download the ansible playbooks and artifacts for this host
git clone https://gitlab.com/i3737/demos-and-training/azure/ansible-example-azure.git

# run the downloaded playbook locally
ansible-playbook --connection=local --inventory=127.0.0.1, --limit=127.0.0.1 ansible-example-azure/main.yaml