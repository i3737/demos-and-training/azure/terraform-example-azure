output "ag_fqdn" {
  value = azurerm_public_ip.ag.fqdn
}

output "bastion_fqdn" {
  value = azurerm_public_ip.bastion.fqdn
}
