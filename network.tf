resource "azurerm_network_security_group" "example" {
  name                = "example-security-group"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
}

resource "azurerm_virtual_network" "example" {
  name                = "tf-example-network"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  address_space       = ["10.16.0.0/16"]
  dns_servers         = ["8.8.8.8", "8.8.4.4", "10.16.0.4", "10.16.0.5"]
}

resource "azurerm_subnet" "sub_a" {
  name                 = "subnet_a"
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes     = ["10.16.0.0/24"]
}
resource "azurerm_subnet" "sub_ag" {
  name                 = "subnet_app_gateway"
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes     = ["10.16.1.0/24"]
}


